public class SensorController {
    public IDoorSensor doorSensor;
    public IHeaterSensor heaterSensor;

    public SensorController(ISensorFactory factory) {
        doorSensor = factory.createDoorSensor();
        heaterSensor = factory.createHeaterSensor();
    }

    public void start() {
        doorSensor.detect();
        heaterSensor.measure();
    }
}
