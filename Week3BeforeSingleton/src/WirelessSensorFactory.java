public class WirelessSensorFactory implements ISensorFactory {
    public WirelessSensorFactory() {
        System.out.println("WIRELESS Sensor Factory \n=======================");
    }

    @Override
    public IDoorSensor createDoorSensor() {
        return new WirelessDoorSensor();
    }

    @Override
    public IHeaterSensor createHeaterSensor() {
        return new WirelessHeaterSensor();
    }
}
