public class WiredSensorFactory implements ISensorFactory {
    public WiredSensorFactory() {
        System.out.println("WIRED Sensor Factory \n=======================");
    }

    @Override
    public IDoorSensor createDoorSensor() {
        return new WiredDoorSensor();
    }

    @Override
    public IHeaterSensor createHeaterSensor() {
        return new WiredHeaterSensor();
    }
}
