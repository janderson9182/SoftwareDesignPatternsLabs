public class WiredDoorSensor implements IDoorSensor {
    @Override
    public void detect() {
        System.out.println("Wired Door is detected as open");
    }
}
