public class WirelessDoorSensor implements IDoorSensor {
    @Override
    public void detect() {
        System.out.println("Wireless Door is detected as closed");
    }
}
