public class MediaPlayer {
    public void workOutWhatToPlayThenPlay(String mediaType) {
        try {
            Media media = (Media) Class.forName(mediaType).newInstance();
            media.play();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}