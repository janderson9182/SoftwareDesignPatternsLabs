public enum EntryNumberMultiton {
    ENTRY_NUMBER, REFERENCE_NUMBER;
    private int entryNumber = 999;
    private int referenceNumber = 99999;

    public synchronized int getNextEntryNumber() {
        return ++entryNumber;
    }

    public synchronized int getNextReferenceNumber() {
        return --referenceNumber;
    }
}
