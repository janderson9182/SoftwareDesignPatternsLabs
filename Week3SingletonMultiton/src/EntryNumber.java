public class EntryNumber {
    private static EntryNumber entryNumber;
    public int currentNumber;

    private EntryNumber() {
        this.currentNumber = 999;
    }

    public static EntryNumber getInstance() {
        if (entryNumber == null) {
            entryNumber = new EntryNumber();
        }
        entryNumber.currentNumber++;
        return entryNumber;
    }

    @Override
    public String toString() {
        return "CurrentNumber: " + currentNumber;
    }
}
