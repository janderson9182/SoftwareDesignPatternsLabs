public class WirelessSensorFactory implements ISensorFactory {
    private WirelessSensorFactory() {
        System.out.println("WIRELESS Sensor Factory \n=======================");
    }

    private static WirelessSensorFactory wirelessSensorFactory;

    public static WirelessSensorFactory getWirelessFactory() {
        if (wirelessSensorFactory == null) {
            System.out.println("Creating single instance of wireless sensor factory");
            wirelessSensorFactory = new WirelessSensorFactory();
        }
        return wirelessSensorFactory;
    }

    @Override
    public IDoorSensor createDoorSensor() {
        return new WirelessDoorSensor();
    }

    @Override
    public IHeaterSensor createHeaterSensor() {
        return new WirelessHeaterSensor();
    }
}
